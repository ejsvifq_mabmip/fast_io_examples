#include"../../include/fast_io.h"
#include"../../include/fast_io_device.h"
#include"../../include/fast_io_legacy.h"
#include"../timer.hh"

int main()
{
	println("fast_io::ogb18030_file:",fast_io::manipulators::chvw(typeid(fast_io::ogb18030_file).name()));
	println("fast_io::igb18030_file:",fast_io::manipulators::chvw(typeid(fast_io::igb18030_file).name()));
}
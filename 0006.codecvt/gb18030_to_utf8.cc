#include"../../include/fast_io.h"
#include"../../include/fast_io_device.h"
#include"../../include/fast_io_legacy.h"
#include"../timer.hh"

int main()
{
	fast_io::timer t(u8"codecvt file");
	fast_io::igb18030_file ibf(U"gb18030_exec.txt");
	fast_io::native_file nf("gb18030_to_utf8_ed.txt",fast_io::open_mode::out);
	transmit(nf,ibf);
}

#include"../../include/fast_io.h"
#include"../../include/fast_io_device.h"
#include"../../include/fast_io_legacy.h"
#include"../timer.hh"

int main()
{
	fast_io::timer t(u8"UTF-8");
	fast_io::u8obuf_file obf(U"utf8_native.txt");
	for(std::size_t i{};i!=10000000;++i)
		print(obf,u8"新闻地图直播视频贴吧学术更多\n");
}

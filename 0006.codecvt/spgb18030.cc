#include"../../include/fast_io.h"
#include"../../include/fast_io_device.h"
#include"../../include/fast_io_legacy.h"
#include"../timer.hh"

int main()
{
	fast_io::timer t(u8"utf8 code cvt to gb18030");
	fast_io::u8ogb18030_file obf(U"gb18030_codecvt.txt");
	for(std::size_t i{};i!=10000000;++i)
		print(obf,u8"hello world hello world\n");
}

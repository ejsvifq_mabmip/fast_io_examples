#include"../../include/fast_io.h"
#include"../../include/fast_io_device.h"
#include"../../include/fast_io_driver/posix_iconv.h"
#include"../timer.hh"

int main()
{
	fast_io::timer t(u8"utf8 code cvt to gb18030");
	fast_io::posix_iconv_file picf("GB18030","UTF-8");
	fast_io::u8oiconv_file oicf(
	{.external_decorator=picf}
	,"test.txt");
	for(std::size_t i{};i!=10000000;++i)
		print(oicf,u8"hello world hello world\n");
}
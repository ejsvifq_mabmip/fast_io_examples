#include"../../include/fast_io.h"
#include"../../include/fast_io_device.h"
#include"../../include/fast_io_driver/posix_iconv.h"

int main()
{
	fast_io::posix_iconv_file picf("GB18030","UTF-8");
	fast_io::u8oiconv_file oicf(
	{.external_decorator=picf}
	,"test.txt");
	for(std::size_t i{};i!=10000000;++i)
		print(oicf,u8"新闻地图直播视频贴吧学术更多\n");
}
#include"../../include/fast_io.h"
#include"../../include/fast_io_device.h"
#include"../../include/fast_io_legacy.h"
#include"../timer.hh"

int main()
{
	fast_io::timer t(u8"codecvt file");
	fast_io::native_file nf("utf8_native.txt",fast_io::open_mode::in);
	fast_io::ogb18030_file obf(U"utf8_to_gb18030_ed.txt");
	transmit(obf,nf);
}

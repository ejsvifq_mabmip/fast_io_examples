#pragma once

namespace fast_io
{

    class timer
    {
        std::u8string_view s;
        unix_timestamp t0;
    public:
        explicit timer(std::u8string_view strvw) : s(strvw), t0(posix_clock_gettime(posix_clock_id::monotonic)) {}
        timer(const timer &) = delete;
        timer &operator=(const timer &) = delete;
        ~timer()
        {
            println(fast_io::native_stderr<char8_t>(),s, u8":", manipulators::fixed(posix_clock_gettime(posix_clock_id::monotonic)-t0,3));
        }
    };
} // namespace fast_io

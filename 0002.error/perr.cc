#include <fast_io.h>

int main()
{
	print(fast_io::err(), "using println\n"); // this prints to fast_io::err();
	perr("using perr\n"); // this prints to fast_io::err();
}
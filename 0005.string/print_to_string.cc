#include <fast_io.h>

int main() 
{
	std::string str;
	fast_io::ostring_ref ref{&str};
	print(ref, "Hello World ", 40, " ok ", 4.32535);
	println("result of do concat with print(): ", str);
}
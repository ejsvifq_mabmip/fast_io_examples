#include <fast_io.h>

int main()
{
	char const *ptr = "This is a char const *";
	println("print bare pointer: ", ptr);
	println("print bare pointer with chvw: ", fast_io::mnp::chvw(ptr));
}
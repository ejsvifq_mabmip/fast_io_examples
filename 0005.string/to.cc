#include <fast_io.h>

int main()
{
	std::string str{"8192"};
	auto result = fast_io::to<int32_t>(str);
	print("The conversion result of string [", str, "] is [", result, "]\n");
}
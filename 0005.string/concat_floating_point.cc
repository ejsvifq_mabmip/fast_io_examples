#include <fast_io.h>

int main()
{
	double PI{3.1415926535};
	std::string str{"I'm a string! "};
	std::string result{fast_io::concat("str value: ", str, "\nPI value: ", PI)};
	println(result);
}
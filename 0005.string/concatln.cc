#include <fast_io.h>

int main()
{
	std::string str1{fast_io::concat("This is a string. ", "Result is ", 42, ".")}; // notice this concat
	println(str1); // notice this println


	std::string str2{fast_io::concatln("This is a string. ", "Result is ", 42, ".")}; // notice this concatln
	print(str2);  // notice this print

}
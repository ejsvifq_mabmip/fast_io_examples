#include <fast_io.h>

int main()
{
    char ch = 'A';
    int8_t u8_value = ch;
    println("print char:   ", ch,
          "\nprint int8_t: ", u8_value);
    println("print with chvw(): ", fast_io::mnp::chvw(ch));
}
#include <fast_io.h>

int main()
{
	std::string str{fast_io::concat("This is a string. ", "Result is 42.")};
	println(str);

	println(fast_io::concat("The result is ", 42));
}